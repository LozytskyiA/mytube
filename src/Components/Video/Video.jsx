import React from 'react';
import { Link } from 'react-router-dom';
import propTypes from 'prop-types';
import moment from 'moment';
import './Video.scss';

export default function Video({ item, className, searchQuery }) {
  const date = moment(item.snippet.publishedAt).fromNow();
  const title = item.snippet.title
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, "'")
    .replace(/&amp;/g, '&');

  return (
    <>
      <Link to={`watch/${item.id.videoId}/${searchQuery}`} className={`${className}`}>
        <img className="video__img" src={item.snippet.thumbnails.medium.url} alt="no comments" />
        <div className="video__content">
          <div className="video__title">{title}</div>
          <div className="video__info">
            <span className="video__channel">{item.snippet.channelTitle}</span>
            <span className="video__date">{date}</span>
          </div>
        </div>
      </Link>
    </>
  );
}

Video.propTypes = {
  item: propTypes.shape().isRequired,
  searchQuery: propTypes.string.isRequired,
  className: propTypes.string.isRequired,
};
