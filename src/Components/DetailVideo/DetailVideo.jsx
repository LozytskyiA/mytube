import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import './DetailVideo.scss';

import apiService from '../../Services/APIService';

export default function DetailVideo(props) {
  const [videos, setVideos] = useState([]);
  const { match } = props;
  const { id } = match.params;
  const { query } = match.params;

  useEffect(() => {
    apiService.getVideos({ q: query }).then(({ data: { items } }) => setVideos(items));
  }, [query]);

  const video = videos.filter((item) => item.id.videoId === id);
  const obj = video[0];
  console.log(obj.snippet);

  return (
    <section className="video-detail container">
      <div className="video-detail__main">
        <iframe
          title="video"
          width="560"
          height="315"
          src={`https://www.youtube.com/embed/${id}`}
          frameBorder="0"
          allow="accelerometer;
          autoplay;
          encrypted-media;
          gyroscope;
          picture-in-picture"
          allowFullScreen
        />
      </div>

      <div className="video-detail__info">
        <div className="video__title">{}</div>
      </div>
    </section>
  );
}

DetailVideo.defaultProps = {
  id: '',
};

DetailVideo.propTypes = {
  id: propTypes.string,
  match: propTypes.shape().isRequired,
};
