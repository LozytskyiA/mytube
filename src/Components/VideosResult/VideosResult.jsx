import React, { useState, useEffect } from 'react';
import propTypes from 'prop-types';
import './VideosResult.scss';

import Video from '../Video/Video';
import apiService from '../../Services/APIService';

export default function VideosResult({ location }) {
  const [videos, setVideos] = useState([]);
  const searchParams = new URLSearchParams(location.search);
  const searchQuery = searchParams.get('search_query');

  useEffect(() => {
    apiService.getVideos({ q: searchQuery })
      .then(({ data: { items } }) => setVideos(items));
    return () => {
      apiService.getVideos({ q: searchQuery })
        .then(({ data: { items } }) => setVideos(items));
    };
  }, [searchQuery]);

  return (
    <div className="videos-result container">
      {
        videos.map((item) => <Video className="videos-result__item" key={item.id.videoId} item={item} searchQuery={searchQuery} />)
      }
    </div>
  );
}

VideosResult.propTypes = {
  location: propTypes.shape().isRequired,
};
