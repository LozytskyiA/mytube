import React, { useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import './Header.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

export default function Header() {
  const history = useHistory();
  const [inputValue, setInputValue] = useState('');

  const handleValue = ({ target }) => {
    setInputValue(target.value);
  };

  const addValueToParams = (e) => {
    e.preventDefault();
    if (inputValue.trim() !== '') {
      const urlParams = new URLSearchParams();
      urlParams.append('search_query', inputValue);
      history.push(`/results?${urlParams}`);
    }
  };

  return (
    <header className="header">
      <Link className="logo" to="/">
        <span className="logo__item">My</span>
        <span className="logo__item logo__item_color-red">Tube</span>
      </Link>

      <div className="search-form-wrapper">
        <form className="search-form" onSubmit={(e) => addValueToParams(e)}>
          <input
            value={inputValue}
            type="text"
            className="search-form__input"
            placeholder="Введите запрос"
            onChange={(e) => handleValue(e)}
          />
          <button
            type="submit"
            className="search-form__button"
          >
            <FontAwesomeIcon className="icon-comment" icon={faSearch} size="1x" />
          </button>
        </form>
      </div>

      <div className="user-block">
        <div className="user-account">.</div>
      </div>
    </header>
  );
}
