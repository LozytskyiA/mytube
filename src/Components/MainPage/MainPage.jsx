import React, { useState, useEffect } from 'react';
import './MainPage.scss';

import Video from '../Video/Video';
import apiService from '../../Services/APIService';

export default function MainPage() {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    apiService.getVideos().then(({ data: { items } }) => setVideos(items));
  }, []);

  return (
    <section className="videos container">
      {
        videos.map((item) => <Video className="videos__item" key={item.id.videoId} item={item} />)
      }
    </section>
  );
}
