import axios from 'axios';

const apiService = {
  getVideos(obj) {
    return axios.get('https://www.googleapis.com/youtube/v3/search', {
      params: {
        part: 'snippet',
        key: 'AIzaSyBpo0unJtFYqnQHjaR7hzovUKiBNFBnqAM',
        maxResults: 32,
        type: 'video',
        ...obj,
      },
    })
      .then((data) => data);
  },
};

export default apiService;
