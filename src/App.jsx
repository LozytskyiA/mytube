import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';

import Header from './Components/Header/Header';
import MainPage from './Components/MainPage/MainPage';
import VideosResult from './Components/VideosResult/VideosResult';
import DetailVideo from './Components/DetailVideo/DetailVideo';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={MainPage} />
        <Route path="/results" component={VideosResult} />
        <Route path="/watch/:id/:query" component={DetailVideo} />
      </Switch>
    </div>
  );
}

export default App;
