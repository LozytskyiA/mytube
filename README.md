# mytube

[API docimentation](https://developers.google.com/youtube/v3/docs/search/list)

`git checkout -b "Название ветки"` - Создать новую ветку и перейти на неё

`git status` - Проверить изменения 

`git add .` - Добавить все изменённые файлы 

`git commit -m "Название коммита"` - Закомитить изменения 

`git pull origin название ветки` - Загрузить последние изменения с ветки 

`git push origin название ветки` - Загрузить изменения на ветку 